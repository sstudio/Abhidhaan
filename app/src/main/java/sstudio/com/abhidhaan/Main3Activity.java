package sstudio.com.abhidhaan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;


public class Main3Activity extends Activity {
    EditText e;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        e = findViewById(R.id.editD);

        CharSequence text = getIntent()
                .getCharSequenceExtra(Intent.EXTRA_PROCESS_TEXT);
        Button button = findViewById(R.id.button4);

        getMean(text.toString());
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMean(e.getText().toString());
            }
        });
    }

    public void getMean(String req) {
        Log.e("req", "" + req);
        EditText edit = findViewById(R.id.editD);
        edit.setText(req);
        Dbassist dbHeplper;
        dbHeplper = new Dbassist(Main3Activity.this);
        boolean langE = true;
        List<String> listUsers = dbHeplper.getAllUsers(req.trim(), langE);
        if (listUsers != null) {
            ListView listviewm = findViewById(R.id.lvdiag);
            ArrayAdapter adapter = new ArrayAdapter<String>(getApplicationContext(),
                    R.layout.layout, R.id.textview2,
                    listUsers);
            listviewm.setAdapter(adapter);
            if (listviewm.getItemAtPosition(0).toString().contains("No results found.")) {
                Toast.makeText(this, "Abhidhaan: No results found.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.push_up_out, R.anim.push_up_in);
        super.onBackPressed();
    }
}
